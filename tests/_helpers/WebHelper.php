<?php
namespace Codeception\Module;

// here you can define custom functions for WebGuy 

class WebHelper extends \Codeception\Module
{
    public function waitForUserInput(){
        if(trim(fgets(fopen("php://stdin","r")))!= chr(13)) return;
    }
}
