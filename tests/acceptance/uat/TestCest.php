<?php
namespace uat;
use \WebGuy;

class TestCest
{

    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function tryToTest(WebGuy $I) {
        $I->amOnPage('/');
        $I->wantTo('check if Test Suite is working');
    }

}