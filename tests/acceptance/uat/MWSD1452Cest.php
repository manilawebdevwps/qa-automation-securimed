<?php
namespace uat;
use \WebGuy;

class MWSD1452Cest
{

    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function checkInformationCookieBar(WebGuy $I) {
        $I->amOnPage('/');
        $I->canSeeCookie('__utma');
        $I->canSeeElement('#cookiepolicybar');
    }

}